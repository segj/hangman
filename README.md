# Hangman game in Python

A game of hangman played on the terminal. The program selects a random word  that is at least 6 characters long and only includes letters A-Z. The player attempts to guess the word letter by letter. The player loses if they make 10 mistakes. Warning: default English dictionary used includes some weird and obsolete words.

Default list of 69903 English words available at:
http://www-personal.umich.edu/~jlawler/wordlist.html

You can play the game with your own custom word lists. Just make a txt file with one word on each line and put it in the same folder where hangman.py is. The game lets you specify which list of words you want to use. Lists with non-english characters may cause problems.

The game is case-insensitive.

Requires Python 3.

