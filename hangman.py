#!/usr/bin/env python3
import random
import os
import sys


class Gamestate:
	
	word_list = []
	secret_word = ""
	guesses_correct = []
	guesses_wrong = []


# this clears the terminal window in both windows and linux
def clear_screen():
	
	os.system("cls||clear")


def read_file(filename):

	items = []
	file_object = open(filename, "r")

	for line in file_object:
		items.append(line.strip())

	file_object.close
	return items


def draw_ascii_art(case):

	if case == 0:
		print("\n\n\n\n\n\n\n\n\n\n")
	elif case == 1:
		print("\n\n\n\n\n\n\n\n\n\n      ___|__")
	elif case == 2:
		print("\n\n         |\n         |\n         |\n         |\n         |\n         |\n         |\n         |\n      ___|__")
	elif case == 3:
		print("\n\n         |\n         |\n         |\n         |\n         |\n         |\n         |\n        /|\\\n      _/_|_\\_")
	elif case == 4:
		print("\n _________\n         |\n         |\n         |\n         |\n         |\n         |\n         |\n        /|\\\n      _/_|_\\_")
	elif case == 5:
		print("\n _________\n       \\ |\n        \\|\n         |\n         |\n         |\n         |\n         |\n        /|\\\n      _/_|_\\_")
	elif case == 6:
		print("\n _________\n |     \\ |\n |      \\|\n         |\n         |\n         |\n         |\n         |\n        /|\\\n      _/_|_\\_")
	elif case == 7:
		print("\n _________\n |     \\ |\n |      \\|\n( )      |\n         |\n         |\n         |\n         |\n        /|\\\n      _/_|_\\_")
	elif case == 8:
		print("\n _________\n |     \\ |\n |      \\|\n( )      |\n |       |\n |       |\n         |\n         |\n        /|\\\n      _/_|_\\_")
	elif case == 9:
		print("\n _________\n |     \\ |\n |      \\|\n( )      |\n |       |\n |       |\n/ \\      |\n         |\n        /|\\\n      _/_|_\\_")
	elif case == 10:
		print("\n _________\n |     \\ |\n |      \\|\n( )      |\n/|\\      |\n |       |\n/ \\      |\n         |\n        /|\\\n      _/_|_\\_")


def print_gamestate(state, mistakes):

	for character in state.secret_word:

		if character in state.guesses_correct:
			print(character, end="")

		else:
			print("_", end="")

	print("\n")

	if mistakes > 0:
		print("Wrong guesses:")

		for character in state.guesses_wrong:
			print(character, end="")
			
		print()


def random_secret_word(state):

	state.secret_word = random.choice(state.word_list).upper()

	if len(state.secret_word) < 6 or not state.secret_word.isalpha():
		random_secret_word(state)


def initialize(state):
	
	state.guesses_correct = []
	state.guesses_wrong = []

	random_secret_word(state)

	clear_screen()
	draw_ascii_art(0)
	print_gamestate(state, 0)


def new_game_prompt(state):

	choice = input("Do you want to try again? (y/n)\n> ").upper()

	if choice == "Y":
		initialize(state)

	elif choice == "N" or choice == "EXIT" or choice == "QUIT":
		sys.exit()

	else:
		print("Invalid input")
		new_game_prompt(state)


def check_victory(state):
	
	for character in state.secret_word:
		if not character in state.guesses_correct:
			return False
	
	return True


def guess(state):

	character = input("\nMake a guess\n> ").upper()

	if character == "EXIT" or character == "QUIT":
		sys.exit()

	elif len(character) != 1:
		print("Guess must be a single character")
		print("Quit or Exit to quit game now")
		guess(state)

	elif not character.isalpha():
		print("Guess must be a character a-z")
		guess(state)

	elif character in state.guesses_wrong or character in state.guesses_correct:
		print("You have already guessed that character!")
		guess(state)

	elif character in state.secret_word:
		state.guesses_correct.append(character)

	else:
		state.guesses_wrong.append(character)


def gameloop(state):

	initialize(state)

	while True:

		guess(state)
		mistakes = len(state.guesses_wrong)

		clear_screen()
		draw_ascii_art(mistakes)
		print_gamestate(state, mistakes)

		if mistakes == 10:
			print("\nYOU LOST!")
			print("The secret word was", state.secret_word)
			new_game_prompt(state)

		elif check_victory(state):
			print("\nYOU WON!")
			new_game_prompt(state)


def main():

	state = Gamestate()

	clear_screen()
	print("Welcome to hangman")

	print("\nType the full filename of the word list you want to use"
	     + "\nEmpty input for default english")
	filename = input("> ")

	if filename == "":
		filename = "jlawler_english_word_list.txt"
	
	try:
		state.word_list = read_file(filename)
		gameloop(state)

	except IOError:
		print("Reading data from " + filename + " failed")


main()

